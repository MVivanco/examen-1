package com.example.cursoandroid.examen1;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    private TextView lblNombre;
    private EditText txtNom;
    private Button btnSalir, BtnEntrar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        txtNom = (EditText) findViewById(R.id.txtNombre);
        btnSalir = (Button) findViewById(R.id.btnSalir);
        BtnEntrar = (Button) findViewById(R.id.btnEntrar);

        btnSalir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        BtnEntrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(txtNom.getText().toString().matches("")){
                    Toast.makeText(MainActivity.this, "Introduce un nombre", Toast.LENGTH_SHORT).show();
                    return;
                }
                Intent intent = new Intent(MainActivity.this, RectanguloActivity.class);
                intent.putExtra("nombre", txtNom.getText().toString());
                startActivity(intent);
            }
        });
    }
}
