package com.example.cursoandroid.examen1;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class RectanguloActivity extends AppCompatActivity {
    private Rectangulo rectangulo;
    private TextView lblNombre, lblPerimetro, lblArea;
    private EditText txtBase, txtAltura;
    private Button btnCalcular, btnLimpiar, btnRegresar;
    private String nombre;
    private Bundle datos;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rectangulo);
        datos = getIntent().getExtras();
        nombre = datos.getString("nombre");

        btnCalcular = (Button) findViewById(R.id.btnCalcular);
        btnLimpiar = (Button) findViewById(R.id.btnLimpiar);
        btnRegresar = (Button) findViewById(R.id.btnRegresar);

        lblNombre = (TextView) findViewById(R.id.lblNombre);
        lblPerimetro = (TextView) findViewById(R.id.lblPerimetro);
        lblArea = (TextView) findViewById(R.id.lblArea);
        txtBase = (EditText) findViewById(R.id.txtBase);
        txtAltura = (EditText) findViewById(R.id.txtAltura);
        lblNombre.setText("Mi nombre es " + nombre);

        btnCalcular.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(txtBase.getText().toString().matches("") || txtAltura.getText().toString().matches("")){
                    Toast.makeText(RectanguloActivity.this, "Ingrese los datos correctamente", Toast.LENGTH_SHORT);
                    return;
                }
                rectangulo = new Rectangulo();

                int base = Integer.parseInt(txtBase.getText().toString());
                int altura = Integer.parseInt(txtAltura.getText().toString());
                rectangulo.setAltura(altura);
                rectangulo.setBase(base);
                lblArea.setText("Area: " + rectangulo.calcularArea());
                lblPerimetro.setText("Perimetro: " + rectangulo.calcularPagPerimetro());
            }
        });

        btnLimpiar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                lblArea.setText("Area:");
                lblPerimetro.setText("Perimetro:");
                rectangulo = new Rectangulo();
                txtAltura.setText("");
                txtBase.setText("");
            }
        });

        btnRegresar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }


}
