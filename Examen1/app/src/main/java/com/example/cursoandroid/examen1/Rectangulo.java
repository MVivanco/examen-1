package com.example.cursoandroid.examen1;

public class Rectangulo {
    private int Base;
    private int Altura;

    public Rectangulo(int base, int altura){
        this.Base = base;
        this.Altura = altura;
    }

    public Rectangulo(){

    }

    public int getBase() {
        return Base;
    }

    public void setBase(int base) {
        Base = base;
    }

    public int getAltura() {
        return Altura;
    }

    public void setAltura(int altura) {
        Altura = altura;
    }

    public float calcularArea(){
        return this.getBase() * this.getAltura();
    }

    public float calcularPagPerimetro(){
        return (this.getBase() * 2) + (this.getAltura() * 2);
    }
}
